defmodule Lyse do
  @moduledoc """
  Documentation for `Lyse`.
  """

  @spec hello :: :world
  @doc """
  Hello world.

  ## Examples

      iex> Lyse.hello()
      :world

  """
  def hello do
    :world
  end
end
