defmodule Lyse.RPN do
  @moduledoc """
  Reverse polish notation calculator.
  """

  @doc """
  Reads a string into either an integer or a float.

  ## Examples

      iex> Lyse.RPN.read("123")
      123

      iex> Lyse.RPN.read("123.123")
      123.123

  """
  @spec read(binary) :: number
  def read(s) do
    try do
      String.to_float(s)
    catch
      :error, :badarg -> String.to_integer(s)
    end
  end

  @doc """
  Evaluates an expression in reverse polish notation.

  ## Examples

      iex> Lyse.RPN.rpn("10 4 3 + 2 * -")
      -4

      iex> Lyse.RPN.rpn("2.7 ln") == :math.log(2.7)
      true

  """
  @spec rpn(binary) :: number
  def rpn(l) when is_binary(l) do
    [result] = l |> String.split(" ") |> Enum.reduce([], &rpn/2)
    result
  end

  defp rpn("+", [n1, n2 | stack]), do: [n2 + n1 | stack]
  defp rpn("-", [n1, n2 | stack]), do: [n2 - n1 | stack]
  defp rpn("*", [n1, n2 | stack]), do: [n2 * n1 | stack]
  defp rpn("/", [n1, n2 | stack]), do: [n2 / n1 | stack]
  defp rpn("^", [n1, n2 | stack]), do: [:math.pow(n2, n1) | stack]
  defp rpn("ln", [n | stack]), do: [:math.log(n) | stack]
  defp rpn("log10", [n | stack]), do: [:math.log10(n) | stack]
  defp rpn("sum", stack), do: [Enum.reduce(stack, &Kernel.+/2)]
  defp rpn("prod", stack), do: [Enum.reduce(stack, &Kernel.*/2)]
  defp rpn(x, stack), do: [read(x) | stack]
end
