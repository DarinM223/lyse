defmodule Lyse.Road do
  @moduledoc """
  You're on a plane due to land at Heathrow airport
  in the next hours. You have to get to London as fast as
  possible; your rich uncle is dying and you want to be the first
  there to claim dibs on his estate.

  There are two roads going from Heathrow to London and a bunch
  of smaller streets linking them together. Because of speed
  limits and usual traffic, some parts of the roads and smaller
  streets take longer to drive on than others. Before you land,
  you decide to maximize your chances by finding the optimal path
  to his house. Here's the map you've found on your laptop:

  ```
     50     5      40     10
  A ==== + ==== + ==== + ====
         |      |      |
      30 |   20 |   25 |
         |      |      |
  B ==== + ==== + ==== + ====
     10     90     2      8
  ```
  """

  @filename "road.txt"

  @doc """
  Opens and parses the file "road.txt" and finds the
  optimal path with the parsed data.
  """
  @spec main :: {:error, atom} | {:ok, [{atom, integer}]}
  def main() do
    File.open(@filename, [:read], fn file ->
      parsed_vals = file |> parse_map([]) |> group_vals([])
      optimal_path(parsed_vals)
    end)
  end

  defp parse_map(file, build) do
    case IO.read(file, :line) do
      :eof ->
        build

      data ->
        parsed = data |> String.trim() |> String.to_integer()
        parse_map(file, [parsed | build])
    end
  end

  defp group_vals([], acc), do: acc
  defp group_vals([x, b, a | rest], acc), do: group_vals(rest, [{a, b, x} | acc])

  @doc """
  Finds the optimal path given a list of
  tuples {a1, b1, x1} where a1 is the distance of the segment
  of road A, b1 is the distance of the segment of road B, and x1
  is the distance of the road X that joins the road A to road B.

  ## Example

      iex> Lyse.Road.optimal_path([{50, 10, 30}, {5, 90, 20}, {40, 2, 25}, {10, 8, 0}])
      [b: 10, x: 30, a: 5, x: 20, b: 2, b: 8]

  """
  @spec optimal_path([{integer, integer, integer}]) :: [{atom, integer}]
  def optimal_path(road_map) do
    {a, b} = Enum.reduce(road_map, {{0, []}, {0, []}}, &shortest_step/2)

    {_, path} =
      cond do
        hd(elem(a, 1)) != {:x, 0} -> a
        hd(elem(b, 1)) != {:x, 0} -> b
      end

    Enum.reverse(path)
  end

  defp shortest_step({a, b, x}, {{dist_a, path_a}, {dist_b, path_b}}) do
    opt_a1 = {dist_a + a, [{:a, a} | path_a]}
    opt_a2 = {dist_b + b + x, [{:x, x}, {:b, b} | path_b]}
    opt_b1 = {dist_b + b, [{:b, b} | path_b]}
    opt_b2 = {dist_a + a + x, [{:x, x}, {:a, a} | path_a]}
    {min(opt_a1, opt_a2), min(opt_b1, opt_b2)}
  end
end
