# Simple math operations
2 + 15
49 * 100
5 / 2
div(5, 2)
rem(5, 2)

# List operations
# Head of list
hd([1, 2, 3])
# Tail of list
tl([1, 2, 3])
# Pattern matching
[a | b] = [1, 2, 3]
# Consing 1 to empty list
[1 | []]

# List comprehensions
for n <- [1, 2, 3, 4], do: 2 * n
# Comprehension with predicate
for n <- 1..10, rem(n, 2) == 0, do: n
# Comprehension with binary
for <<(x <- <<1, 2, 3, 4, 5>>)>>, do: x

# Module + pattern matching
defmodule Greet do
  def greet(:male, name), do: IO.puts("Hello, Mr. #{name}!")
  def greet(:female, name), do: IO.puts("Hello, Mrs. #{name}!")
  def greet(_, name), do: IO.puts("Hello, #{name}!")
end

Greet.greet(:male, "bob")
Greet.greet(:female, "jane")
Greet.greet(:alien, "bob")

# Module + pattern matching 2
defmodule Functions do
  def head([h | _]), do: h
  def second([_, x | _]), do: x

  def same(x, x), do: true
  def same(_, _), do: false

  def valid_time({date = {y, m, d}, time = {h, min, s}}) do
    IO.puts("The Date tuple (#{inspect(date)}) says today is: #{y}/#{m}/#{d}")
    IO.puts("The time tuple (#{inspect(time)}) indicates: #{h}:#{min}:#{s}")
  end

  def valid_time(_), do: IO.puts("Stop feeding me wrong data!")

  # Guards
  def old_enough(x) when x >= 16, do: true
  def old_enough(_), do: false

  def right_age(x) when x >= 16 and x <= 104, do: true
  def right_age(_), do: false

  # Chains of when can replace or in guards
  def wrong_age(x) when x < 16 when x > 104, do: true
  def wrong_age(_), do: false
end

Functions.head([1, 2, 3, 4])
Functions.second([1, 2, 3, 4])
Functions.same(1, 1)
Functions.same(1, 2)
Functions.valid_time({{2011, 9, 6}, {9, 4, 43}})
Functions.valid_time({{2011, 9, 6}, {9, 4}})

defmodule WhatTheIf do
  # Cond statement
  def help_me(animal) do
    cond do
      animal == :cat -> "meow"
      animal == :beef -> "mooo"
      animal == :dog -> "bark"
      animal == :tree -> "bark"
      true -> "fgdadfgna"
    end
  end

  # Case statements
  def insert(x, []), do: [x]

  def insert(x, set) do
    # Could also use Enum.member?
    case x in set do
      true -> set
      false -> [x | set]
    end
  end

  def beach(temperature) do
    case temperature do
      {:celsius, n} when n >= 20 and n <= 45 -> "favorable"
      {:kelvin, n} when n >= 293 and n <= 318 -> "scientifically favorable"
      {:fahrenheit, n} when n >= 68 and n <= 113 -> "favorable in the US"
      _ -> "avoid beach"
    end
  end
end

# Type conversions:
String.to_integer("54")
Integer.to_string(54)
# should throw exception
String.to_integer("54.32")
String.to_float("54.32")
Atom.to_string(true)
List.to_string('hi there')
String.to_charlist("hi there")

defmodule Recursive do
  def fac(0), do: 1
  def fac(n) when n > 0, do: n * fac(n - 1)

  def len([]), do: 0
  def len([_ | t]), do: 1 + len(t)

  def tail_fac(n), do: tail_fac(n, 1)
  defp tail_fac(0, acc), do: acc
  defp tail_fac(n, acc) when n > 0, do: tail_fac(n - 1, n * acc)

  def tail_len(l), do: tail_len(l, 0)
  defp tail_len([], acc), do: acc
  defp tail_len([_ | t], acc), do: tail_len(t, acc + 1)

  def duplicate(0, _), do: []
  def duplicate(n, term) when n > 0, do: [term | duplicate(n - 1, term)]

  def tail_duplicate(n, term), do: tail_duplicate(n, term, [])
  defp tail_duplicate(0, _, l), do: l
  defp tail_duplicate(n, term, l) when n > 0, do: tail_duplicate(n - 1, term, [term | l])
end

Recursive.fac(4)
Recursive.len([1, 2, 3, 4])
Recursive.tail_fac(4)
Recursive.tail_len([1, 2, 3, 4])

defmodule Tree do
  def empty(), do: {:node, nil}

  def insert(k, v, {:node, nil}), do: {:node, {k, v, {:node, nil}, {:node, nil}}}

  def insert(nk, nv, {:node, {k, v, smaller, larger}}) when nk < k,
    do: {:node, {k, v, insert(nk, nv, smaller), larger}}

  def insert(nk, nv, {:node, {k, v, smaller, larger}}) when nk > k,
    do: {:node, {k, v, smaller, insert(nk, nv, larger)}}

  def insert(k, v, {:node, {k, _, smaller, larger}}), do: {:node, {k, v, smaller, larger}}

  def lookup(_, {:node, nil}), do: {:error, :undefined}
  def lookup(k, {:node, {k, v, _, _}}), do: {:ok, v}
  def lookup(k, {:node, {nk, _, smaller, _}}) when k < nk, do: lookup(k, smaller)
  def lookup(k, {:node, {_, _, _, larger}}), do: lookup(k, larger)
end

t1 = Tree.insert("Jim Woodland", "jim.woodland@gmail.com", Tree.empty())
t2 = Tree.insert("Mark Anderson", "i.am.a@hotmail.com", t1)

addresses =
  Tree.insert(
    "Anita Bath",
    "abath@someuni.edu",
    Tree.insert(
      "Kevin Robert",
      "myfairy@yahoo.com",
      Tree.insert(
        "Wilson Longbrow",
        "longwil@gmail.com",
        t2
      )
    )
  )

Tree.lookup("Anita Bath", addresses)
Tree.lookup("Jacques Requin", addresses)

# Higher order functions
defmodule HHFuns do
  def one(), do: 1
  def two(), do: 2

  def add(x, y), do: x.() + y.()
end

HHFuns.add(&HHFuns.one/0, &HHFuns.two/0)

prepare_alarm = fn room ->
  IO.puts("Alarm set in #{room}.")
  fn -> IO.puts("Alarm tripped in #{room}! Call Batman!") end
end

alarm_ready = prepare_alarm.("bathroom")
alarm_ready.()

:erlang.error(:badarith)
:erlang.error(:custom_error)
throw :permission_denied
:erlang.throw(:permission_denied)

defmodule Exceptions do
  def throws(f) do
    try do
      f.()
    catch
      throws -> {:throw, :caught, throws}
    end
  end
end
Exceptions.throws(fn -> throw(:thrown) end)
Exceptions.throws(fn -> :erlang.error(:pang) end)

# Catch throws, errors, and exits
defmodule Exceptions do
  def sword(1), do: throw(:slice)
  def sword(2), do: :erlang.error(:cut_arm)
  def sword(3), do: exit(:cut_leg)
  def sword(4), do: throw(:punch)
  def sword(5), do: exit(:cross_bridge)

  def talk(), do: "blah blah"

  def black_knight(attack) when is_function(attack, 0) do
    try do
      attack.()
      "None shall pass."
    catch
      :throw, :slice -> "It is but a scratch."
      :error, :cut_arm -> "I've had worse."
      :exit, :cut_leg -> "Come on you pansy!"
      _, _ -> "Just a flesh wound."
    end
  end
end
Exceptions.talk()
Exceptions.black_knight(&Exceptions.talk/0)
Exceptions.black_knight(fn -> Exceptions.sword(1) end)
Exceptions.black_knight(fn -> Exceptions.sword(2) end)
Exceptions.black_knight(fn -> Exceptions.sword(3) end)
Exceptions.black_knight(fn -> Exceptions.sword(4) end)
Exceptions.black_knight(fn -> Exceptions.sword(5) end)

# catch expression doesn't exist in Elixir, so uses macro to mock it.
defmodule Catch do
  defmacro expr_catch(expr) do
    quote do
      try do: unquote(expr), catch: (a, b -> {a, b})
    end
  end
end
require Catch
Catch.expr_catch(throw(:whoa))
Catch.expr_catch(exit(:die))
Catch.expr_catch(1 / 0)
Catch.expr_catch(2 + 2)
