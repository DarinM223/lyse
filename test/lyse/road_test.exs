defmodule Lyse.RoadTest do
  use ExUnit.Case

  doctest Lyse.Road

  test "main/0 properly loads the file" do
    assert {:ok, [b: 10, x: 30, a: 5, x: 20, b: 2, b: 8]} = Lyse.Road.main()
  end
end
