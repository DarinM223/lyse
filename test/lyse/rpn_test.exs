defmodule Lyse.RPNTest do
  use ExUnit.Case

  alias Lyse.RPN

  doctest Lyse.RPN

  test "rpn/1 properly calculates reverse polish notation" do
    assert 5 = RPN.rpn("2 3 +")
    assert 87 = RPN.rpn("90 3 -")
    assert -4 = RPN.rpn("10 4 3 + 2 * -")
    assert -2.0 = RPN.rpn("10 4 3 + 2 * - 2 /")
    assert 4037 = RPN.rpn("90 34 12 33 55 66 + * - + -")
    assert 8.0 = RPN.rpn("2 3 ^")
    assert true = :math.sqrt(2) == RPN.rpn("2 0.5 ^")
    assert true = :math.log(2.7) == RPN.rpn("2.7 ln")
    assert true = :math.log10(2.7) == RPN.rpn("2.7 log10")
    assert 50 = RPN.rpn("10 10 10 20 sum")
    assert 10.0 = RPN.rpn("10 10 10 20 sum 5 /")
    assert 1000.0 = RPN.rpn("10 10 20 0.5 prod")
    assert 28.0 = RPN.rpn("1 2 ^ 2 2 ^ 3 2 ^ 4 2 ^ sum 2 -")
  end

  test "rpn/1 throws :badmatch error when too many numbers left in the stack" do
    badmatch_result =
      try do
        RPN.rpn("90 34 12 33 55 66 + * - +")
      catch
        :error, {:badmatch, [_ | _]} -> :ok
      end

    assert :ok = badmatch_result
  end
end
